import React from 'react';

// Create a Context Object
// A context object as the name states is a data that can be used to store information that can be share to other component/s within the app.

// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing.


// with the help of createContext() method we were able to create a context stored in variable UserContext
const UserContext = React.createContext();

// the 'Provider' component allows other components to consume/use the context object and supply necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;